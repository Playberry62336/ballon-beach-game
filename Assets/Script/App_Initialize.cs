﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class App_Initialize : MonoBehaviour
{
    public AdManagerScript AM;
    public GameObject Ads_Button;
    public GameObject player;
    public GameObject InGameUi_Panel;
    public GameObject InMenuUi_Panel;
    public GameObject GameOverUi_Panel;
    private bool CanPlayerMove = true;
    public bool hasSeenRewarded = false;
       
    public void Awake()
    {
        Shader.SetGlobalFloat("_Curvature", 2.0f);
        Shader.SetGlobalFloat("_Trimming", 0.1f);
        Application.targetFrameRate = 60;
    }
    // Start is called before the first frame update
    void Start()
    {
        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        InGameUi_Panel.gameObject.SetActive(false);
        InMenuUi_Panel.gameObject.SetActive(true);
        GameOverUi_Panel.gameObject.SetActive(false);


    }
    public void OnClickPlay()
    {
        if (CanPlayerMove == true)
        {
            StartCoroutine(PlayGame(1f));
        }
        else
        {
            StartCoroutine(PlayGame(0.0f));
        }
           
    }
    public void OnClickPause()
    {
        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        CanPlayerMove = true;
        InGameUi_Panel.gameObject.SetActive(false);
        InMenuUi_Panel.gameObject.SetActive(true);
        GameOverUi_Panel.gameObject.SetActive(false);
    }
    public void OnClickRestart()
    {
        SceneManager.LoadScene(0);
    }
    public void GameOver()
    {
        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        CanPlayerMove = true;
        InGameUi_Panel.gameObject.SetActive(false);
        InMenuUi_Panel.gameObject.SetActive(false);
        GameOverUi_Panel.gameObject.SetActive(true);

        if(hasSeenRewarded==true)
        {
            Ads_Button.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
            Ads_Button.GetComponent<Button>().enabled = false;
        }
    }


    public void OnClickAd()
    { 
        StartCoroutine(PlayGame(1f));
        AM.ShowInterstitialAd();
    }
    IEnumerator PlayGame(float waitTime)
    {
        InGameUi_Panel.gameObject.SetActive(true);
        InMenuUi_Panel.gameObject.SetActive(false);
        GameOverUi_Panel.gameObject.SetActive(false);
        yield return new WaitForSeconds(waitTime);
        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
        
    }
   
}
