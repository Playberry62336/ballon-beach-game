﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleControllerScript : MonoBehaviour
{
    public GameObject player;
    public GameObject[] triangles;
    Vector3 spawnObstaclePosition;

    // Update is called once per frame
    void Update()
    {
        float distanceToHorizontal = Vector3.Distance(player.transform.position, spawnObstaclePosition);
        if(distanceToHorizontal<120)
        {
            SpawnObsatcle();
        }
      
    }
    public void SpawnObsatcle()
    {
        spawnObstaclePosition = new Vector3(0, 0, spawnObstaclePosition.z + 30);
        Instantiate(triangles[(Random.Range(0, triangles.Length))], spawnObstaclePosition, Quaternion.identity);

    }
}
