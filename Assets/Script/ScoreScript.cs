﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    public int highScore;
    public int score;
    public Text scoreText;
    public Text scoreBoard;

    // Update is called once per frame
    void Update()
    {
       
    }
    public void OnTriggerEnter(Collider collider)
    {
       if( collider.gameObject.CompareTag("ScoreUp"))
        {
            AddScore();
        }
      
    }
    public void AddScore()
    {
        score++;
        scoreText.text = score.ToString();
        
        if(score>highScore)
        {
            PlayerPrefs.SetInt("HighScore", score);
        }
        highScore = PlayerPrefs.GetInt("HighScore");
        scoreBoard.text = highScore.ToString();
    }

}
