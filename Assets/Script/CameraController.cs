﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    public float cameraFollow;


    
    void LateUpdate()
    {
        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position,new Vector3(0,gameObject.transform.position.y,player.transform.position.z-cameraFollow), Time.deltaTime * 100);
    }
}
