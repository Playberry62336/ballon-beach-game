﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject sceneManager;
    public float playerSpeed=500;
    public float directionalSpeed=20;
    public AudioClip scoreUp;
    public AudioClip damageUp;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
       

#if UNITY_EDITOR||UNITY_ANDROID||UNITY_WEBPLAYER

        float horizontalMovement = Input.GetAxis("Horizontal");
        transform.position = Vector3.Lerp(gameObject.transform.position, new Vector3(Mathf.Clamp(gameObject.transform.position.x + horizontalMovement, -2.5f, 2.5f), gameObject.transform.position.y, gameObject.transform.position.z), directionalSpeed * Time.deltaTime);
#endif
        GetComponent<Rigidbody>().velocity = Vector3.forward * playerSpeed * Time.deltaTime;
        transform.Rotate(Vector3.right * GetComponent<Rigidbody>().velocity.z/2);
        //Mobile Controll
        Vector2 touch = Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(0, 0, 10f));
        if(Input.touchCount>0)
        {
            transform.position = new Vector3(touch.x, transform.position.y, transform.position.z);
        }


    }
    public void OnTriggerEnter(Collider collider)
    {
       if( collider.gameObject.CompareTag("ScoreUp"))
        {
            GetComponent<AudioSource>().PlayOneShot(scoreUp, 1f);
        }
        if (collider.gameObject.CompareTag("triangle"))
        {
            GetComponent<AudioSource>().PlayOneShot(damageUp, 1f);
            sceneManager.GetComponent<App_Initialize>().GameOver();
        }

    }
    
}
